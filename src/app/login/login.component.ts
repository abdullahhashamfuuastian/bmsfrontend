import { Component } from '@angular/core';
import { ApiService } from '../service/api.service';
import { ConfirmationService, MessageService } from 'primeng/api';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { CommonService } from '../service/common.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
 
  constructor(private router: Router,private common : CommonService, private api: ApiService, private messageService: MessageService, private confirmationService: ConfirmationService,private authService : AuthService) { }


}
